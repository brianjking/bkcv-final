<!----- License ----->

WEB Résumé is licensed under Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License. 
It's free to use. You can download, edit, customize and host the template anywhere you want. 
However, you are NOT allowed to sell or redistribute WEB Résumé on your own. 

Please keep the footer links intact.

Images are only for preview only. Please avoid using them on your personal page.

Copyright (c) 2015 WEB Resume - www.webresume.net