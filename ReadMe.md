<!----- About ----->

WEB Résumé is an online resume that you can use when applying for jobs, 
showcasing your works or even to let the world know who you really are. 
You can also link to it from social networks to allow your friends a way to learn more about you.

<!----- How to Use ----->

To easily change colors, install the less.min.js > add style.Less file and change the HEX codes in variables.
After editing, compile the .less file using a compiler (eg-: WinLess, PrePros) 

How-To Guide - http://webresume.net/blog/2015/01/how-to-customize/

*You can find instructions for editing each section inside the HTML file
*Edit the "style.css" file to customize the look
*Replace the images inside "images" folder with your photos

<!----- License ----->

WEB Résumé is licensed under Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License. 
It's free to use. You can download, edit, customize and host the template anywhere you want. 
However, you are NOT allowed to sell or redistribute WEB Résumé on your own. 

Please keep the footer links intact.

Copyright (c) 2015 WEB Resume - www.webresume.net
    
<!----- Premium ----->

If you'd like to have free hosting and a vanity URL for your WEB Resume
Try our Premium plans starting at only $5 - www.webresume.net/premium.html

<!----- Contact ----->

For more info and feedback contact - info@webresume.net

<!-- Version History -->

 01-01-2015 v1.0 - Initial release
 27-01-2015 v1.02 - Minor updates to progress circles and portfolio
 02-06-2015 v1.2 - Bootstrap and core file updates, New .Less file for easy color changing
 